# Práctica 4

**Universitario:** Fred Chavez Ruiz  
**Materia:** Diseño y Programación Gráfica

En la presente práctica se aprende a subir imagenes a GitLab mediante un archivo README.md, en la cual se utilizo la pagina web (https://codingfantasy.com/games/css-grid-attack/play) Play Grid Attack, de la cual enviaremos capturas de los niveles: 10, 18, 20, 29, 30, 40 45, 50 60, 66, 70, 78 y 80.

## Capturas 

### Level 10
![**Level 10**](/images_p4/level10_hardMode.png)
### Level 18
![**Level 18**](/images_p4/level18_hardMode.png)
### Level 20
![**Level 20**](/images_p4/level20_hardMode.png)
### Level 29
![**Level 29**](/images_p4/level29_hardMode.png)
### Level 30
![**Level 30**](/images_p4/level30_hardMode.png)
### Level 40
![**Level 40**](/images_p4/level40_hardMode.png)
### Level 45
![**Level 45**](/images_p4/level45_hardMode.png)
### Level 50
![**Level 50**](/images_p4/level50_hardMode.png)
### Level 60
![**Level 60**](/images_p4/level60_hardMode.png)
### Level 66
![**Level 66**](/images_p4/level66_hardMode.png)
### Level 70
![**Level 70**](/images_p4/level70_hardMode.png)
### Level 78
![**Level 78**](/images_p4/level78_hardMode.png)
### Level 80
![**Level 80**](/images_p4/level80_hardMode.png)

